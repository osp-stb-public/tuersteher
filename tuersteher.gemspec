# encoding: utf-8
$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |s|
  s.name        = 'tuersteher'
  s.version     = '1.0.4'
  s.authors     = ["Bernd Ledig"]
  s.email       = ["bernd@ledig.info","bernd.ledig@ottogroup.com"]
  s.homepage    = "https://gitlab.com/osp-stb-public/tuersteher"
  s.summary     = "Access-Handling for Rails-Apps"
  s.description = <<-EOT
    Security-Layer for Rails-Application acts like a firewall.
  EOT
  s.licenses = ["GPL-3.0-or-later"]

  s.rubyforge_project = "tuersteher"

  s.extra_rdoc_files = ["README.rdoc"]
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.required_ruby_version = '> 2.5'

  #s.add_runtime_dependency "i18n"

  s.add_development_dependency "rake", '~> 10.5'
  s.add_development_dependency "rspec", '~> 3.8'

end

