require "spec_helper"

module Tuersteher

  describe AccessRules do

    context 'path_access?' do
      before do
        rules = [
          PathAccessRule.new('/'),
          PathAccessRule.new('/admin').role(:admin),
          PathAccessRule.new('/images').method(:get),
          PathAccessRule.new('/status').method(:get).role(:system)
        ]
        expect(AccessRulesStorage.instance).to receive(:path_rules).at_least(:once){ rules }
        @login_context = double('login_context')
      end


      context "LoginContext with role :user" do

        it "should be true for this paths" do
          expect(AccessRules.path_access?(@login_context, '/', :get)).to be_truthy
          expect(AccessRules.path_access?(@login_context, '/', :post)).to be_truthy
          expect(AccessRules.path_access?(@login_context, '/images', :get)).to be_truthy
        end

        it "should not be true for this paths" do
          expect(@login_context).to receive(:has_role?){|role| role==:user}.at_least(:once)
          expect(AccessRules.path_access?(@login_context, '/admin', :get)).to_not be_truthy
          expect(AccessRules.path_access?(@login_context, '/images', :post)).to_not be_truthy
          expect(AccessRules.path_access?(@login_context, '/status', :get)).to_not be_truthy
        end
      end


      context "LoginContext with role :admin" do
        before do
          expect(@login_context).to receive(:has_role?){|role| role==:admin}.at_least(:once)
        end

        it "should be true for this paths" do
          expect(AccessRules.path_access?(@login_context, '/', :get)).to be_truthy
          expect(AccessRules.path_access?(@login_context, '/admin', :post)).to be_truthy
          expect(AccessRules.path_access?(@login_context, '/images', :get)).to be_truthy
        end

        it "should not be true for this paths" do
          expect(AccessRules.path_access?(@login_context, '/xyz', :get)).to_not be_truthy
          expect(AccessRules.path_access?(@login_context, '/images', :post)).to_not be_truthy
          expect(AccessRules.path_access?(@login_context, '/status', :get)).to_not be_truthy
        end
      end


      context "LoginContext with role :system" do
        before do
          expect(@login_context).to receive(:has_role?){|role| role==:system}.at_least(:once)
        end

        it "should be true for this paths" do
          expect(AccessRules.path_access?(@login_context, '/', :get)).to be_truthy
          expect(AccessRules.path_access?(@login_context, '/status', :get)).to be_truthy
        end

        it "should not be true for this paths" do
          expect(AccessRules.path_access?(@login_context, '/xyz', :get)).to_not be_truthy
          expect(AccessRules.path_access?(@login_context, '/admin', :post)).to_not be_truthy
        end
      end


      context "without user" do
        it "should be true for this paths" do
          expect(AccessRules.path_access?(nil, '/', :get)).to be_truthy
        end

        it "should not be true for this paths" do
          expect(AccessRules.path_access?(nil, '/xyz', :get)).to_not be_truthy
          expect(AccessRules.path_access?(nil, '/admin', :post)).to_not be_truthy
        end
      end
    end


    context 'model_access?' do

      class SampleModel1; end
      class SampleModel2; def owner?(user); false; end; end


      before do
        rules = [
          ModelAccessRule.new(:all).grant.role(:sysadmin),
          ModelAccessRule.new(SampleModel1).grant.method(:all),
          ModelAccessRule.new(SampleModel2).grant.method(:read),
          ModelAccessRule.new(SampleModel2).grant.method(:update).role(:user).extension(:owner?),
          ModelAccessRule.new(SampleModel2).deny.method(:create),
          ModelAccessRule.new(SampleModel2).grant.method(:all).role(:admin),
        ]
        expect(AccessRulesStorage.instance).to receive(:model_rules).at_least(:once){ rules }
        @login_context = double('login_context')
        @model1 = SampleModel1.new
        @model2 = SampleModel2.new
        @model2.stub(:owner?){ false }
      end


      context "LoginContext with role :user" do
        before do
          @login_context.stub(:has_role?){|role| role==:user}
        end

        it "should be true for this rules" do
          expect(AccessRules.model_access?(@login_context, @model1, :xyz)).to be_truthy
          @model2.stub(:owner?){ true }
          expect(AccessRules.model_access?(@login_context, @model2, :read)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :update)).to be_truthy
        end

        it "should not be true for this" do
          expect(AccessRules.model_access?(@login_context, @model2, :update)).to be_falsy
          expect(AccessRules.model_access?(@login_context, @model2, :delete)).to be_falsy
        end
      end


      context "LoginContext with role :admin" do
        before do
          @login_context.stub(:has_role?){|role| role==:admin}
        end

        it "should be true for this" do
          expect(AccessRules.model_access?(@login_context, @model1, :xyz)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :read)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :update)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :delete)).to be_truthy
        end

        it "should not be true for this" do
          expect(AccessRules.model_access?(@login_context, @model2, :create)).to be_falsy
        end
      end


      context "LoginContext with role :sysadmin" do
        before do
          @login_context.stub(:has_role?){|role| role==:sysadmin}
        end

        it "should be true for this" do
          expect(AccessRules.model_access?(@login_context, "test", :xyz)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model1, :xyz)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :read)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :update)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :delete)).to be_truthy
          expect(AccessRules.model_access?(@login_context, @model2, :create)).to be_truthy
        end
      end


      context "without user" do
        it "should be true for this models" do
          expect(AccessRules.model_access?(nil, @model1, :xyz)).to be_truthy
          expect(AccessRules.model_access?(nil, @model2, :read)).to be_truthy
        end

        it "should not be true for this models" do
          expect(AccessRules.model_access?(nil, @model2, :update)).to be_falsy
        end
      end
    end # of context 'model_access?'



    context 'purge_collection' do

      class SampleModel
        def owner?(user); false; end
      end

      before do
        rules = [
          ModelAccessRule.new(SampleModel).method(:update).role(:admin),
          ModelAccessRule.new(SampleModel).method(:update).role(:user).extension(:owner?),
        ]
        AccessRulesStorage.instance.stub(:model_rules).and_return(rules)
        @login_context = double('user')
        @model1 = SampleModel.new
        @model2 = SampleModel.new
        @model3 = SampleModel.new
        @model3.stub(:owner?).and_return(true)
        @collection = [@model1, @model2, @model3]
      end

      it "Should return [@model3] for user with role=:user" do
        @login_context.stub(:has_role?){|role| role==:user}
        expect(AccessRules.purge_collection(@login_context, @collection, :update)).to eq [@model3]
      end

      it "Should return all for user with role=:admin" do
        @login_context.stub(:has_role?){|role| role==:admin}
        expect(AccessRules.purge_collection(@login_context, @collection, :update)).to eq @collection
      end
    end

  end
end
