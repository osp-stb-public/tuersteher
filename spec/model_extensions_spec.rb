require "spec_helper"

module Tuersteher

  describe ModelExtensions do

    class SampleModel
      include ModelExtensions

      def deactived
        check_access :deactived
      end
    end


    before do
      rules = [ModelAccessRule.new(SampleModel).grant.method(:deactived).role(:admin)]
      #AccessRulesStorage.instance.stub(:model_rules){ rules }
      expect(AccessRulesStorage.instance).to receive(:model_rules){ rules }
      @login_context = double('login_context')
      Thread.current[:login_context] = @login_context
    end


    context "check_access" do

      it "should not raise a Error for login_context with role :admin" do
        expect(@login_context).to receive(:has_role?){|role| role==:admin}
        model = SampleModel.new
        model.deactived
      end

      it "should raise a SecurityError for login_context with not role :admin" do
        expect(@login_context).to receive(:has_role?){|role| role==:user}
        model = SampleModel.new
        expect{ model.deactived }.to raise_error(SecurityError)
      end

    end # of context "grant with roles"


    context "purge_collection" do

      it "should purge nothing for login_context with role :admin" do
        expect(@login_context).to receive(:has_role?){|role| role==:admin}
        list = [SampleModel.new]
        expect(SampleModel.purge_collection(list, :deactived)).to eq list
      end

      it "should purge all for login_context with not role :admin" do
        expect(@login_context).to receive(:has_role?){|role| role==:user}
        list = [SampleModel.new]
        expect(SampleModel.purge_collection(list, :deactived)).to eq []
      end

    end # of  context "purge_collection"
  end
end

